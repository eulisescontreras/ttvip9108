// Copyright IBM Corp. 2016,2019. All Rights Reserved.
// Node module: loopback-workspace
// This file is licensed under the MIT License.
// License text available at https://opensource.org/licenses/MIT

'use strict';
var loopback = require("loopback");
var path = require('path');
var app = require('../server');

module.exports = function(server) {
  // Install a `/` route that returns server status
  const router = server.loopback.Router();

  router.use(loopback.token());

  router.get('/', server.loopback.status());

  router.get('/api/images', async function(req, res) {
    res.sendFile(path.resolve(__dirname, '../../uploads', req.query.name + '.png'));
  });

  server.use(router);
};
