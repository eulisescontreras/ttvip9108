var server = require('./server');
var ds = server.dataSources.mysql;

var promises = [];
promises.push(updateTables());

Promise.all(promises).then(() => {
    console.log('Update executed successfully');
}).catch((err) => {
    console.error(err);
}).finally(() => {
    ds.disconnect();
    process.exit();
});

function updateTables() {
    return new Promise(async (resolve, reject) => {
        const tables = ['InfoUser', 'Invoice', 'Referred']

        const roleReferred = await server.models.Role.findOne({where: {name: 'referred' }});
        if (!roleReferred) {
            await server.models.Role.create({ name: 'referred' });
        }

        await ds.autoupdate(tables, function (er) {
            if (er) throw er;
            console.log('Loopback tables update in ', ds.adapter.name);
            resolve(true);
        });
    });
}