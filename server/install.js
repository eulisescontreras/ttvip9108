var server = require('./server');
var ds = server.dataSources.mysql;
var lbTables = ['user', 'AccessToken', 'ACL', 'RoleMapping', 'Role', 'InfoUser'];

var promises = [];
promises.push(createLbTables());

Promise.all(promises).then(() => {
  console.log('Loopback migration executed successfully');
}).catch((err) => {
  console.error(err);
}).finally(() => {
	ds.disconnect();
	process.exit();
});

function createLbTables() {
	return new Promise((resolve,reject) => {

      ds.automigrate(lbTables, async function(er) {
        if (er) throw er;
        console.log('Loopback tables [' + lbTables + '] created in ', ds.adapter.name);
  
          // Crear usuarios
          let users = await server.models.User.create([
            {id: 1, username: 'Administrador', firstName: "User", verificationCodePassword:"U29ycmVudG8yMDIw", lastName: "Administrador", birthdate: Date('1994-01-10'), email: 'villegasvictorjose@gmail.com', password: 'Sorrento2020', emailVerified: 1, typeAccess: 'admin'},
            {id: 2, username: 'Ttvip', firstName: "User", lastName: "Ttvip", birthdate: Date('1994-01-10'), email: 'vvillegasfacyt@gmail.com', password: 'admin', emailVerified: 1, typeAccess: 'root'}
          ]);

          // Crear roles
          let roleAdmin = await server.models.Role.create(
            { name: 'admin' }
          );
          let roleManager = await server.models.Role.create(
            { name: 'manager' }
          );
  
          // Asignar roles para cada usuario
          await roleAdmin.principals.create({
              principalType: server.models.RoleMapping.USER,
              principalId: 1
          });
          await roleManager.principals.create({
              principalType: server.models.RoleMapping.USER,
              principalId: 2
          });

          // Create defaults access tokens
          await server.models.AccessToken.create([{
            userId: 1,
            id: 'YVy4eqrYXJpCik8OHhn7yCEFAG9mQPIoKySAkbQGHB5lFaALO6BlZHm7LmBeY2I1',
            ttl: -1,
            created: Date()
          }]);

          resolve(true);
  
      });

	});
}