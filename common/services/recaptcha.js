var axios = require('axios');
var config = require('../../server/config.json');

var secret = '6LcGcpgaAAAAAECs2djqIhZ-knRbn7P0D8tKFbhm';

const recaptcha = {
    verify: function(token) {
        return axios.get("https://www.google.com/recaptcha/api/siteverify?secret=" + secret + "&response=" + token);
    }
}

module.exports = recaptcha;