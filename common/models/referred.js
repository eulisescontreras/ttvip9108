'use strict';

var app = require('../../server/server');

module.exports = function(Referred) {

    Referred.afterRemote('create', async function(context, referred, next) {
        let user = await app.models.user.create({
            "firstName": referred.firstName,
            "lastName": referred.lastName,
            "unlock": true,
            "password": referred.password,
            "typeAccess": "referred",
            "username": referred.username,
            "email": referred.email,
            "emailVerified": true,
            "id": null
        });
        let roleReferred = await app.models.Role.findOne({where: {name: 'referred' }});
        
        await roleReferred.principals.create({
            principalType: app.models.RoleMapping.USER,
            principalId: user.id
        });
        let referredUser = await app.models.Referred.findById(referred.id);
        referredUser.updateAttributes({userId: user.id});
    });

};