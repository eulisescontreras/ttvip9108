'use strict';

var app = require('../../server/server');

module.exports = function(User) {

    User.remoteMethod('getUsers', {
        http: {path: '/getUsers', verb: 'get'},
        accepts: [
            { arg: 'req', type: 'object', http: {source: 'req'} },
            { arg: 'res', type: 'object', http: { source: 'res' } },
        ],
        returns: {type: 'object', root: true}
    });
    User.getUsers = async function (req,res) {
        let users;
        try{
            users = await app.models.User.find({include: ["roles"]});

        }catch(err){
            throw new Error('ERROR');
        }
        return users;
    }
};