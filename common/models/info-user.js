'use strict';

var loopback = require("loopback");
var app = require('../../server/server');
var path = require('path');
var fs = require('fs');
var pdf = require('html-pdf');
var recaptcha = require('../services/recaptcha.js');

const urlApi = 'https://ttvip9108.com';
module.exports = function (Infouser) {

    Infouser.remoteMethod('register', {
        http: { path: '/register', verb: 'post' },
        accepts: [
            { arg: 'req', type: 'object', http: { source: 'req' } },
            { arg: 'body', type: 'object', http: { source: 'body' } },
            { arg: 'res', type: 'object', http: { source: 'res' } }
        ],
        returns: { type: 'object', root: true }
    });
    Infouser.register = async function (req, body, res) {

        try {
            let res = await recaptcha.verify(body.recaptcha);
            if (res.data.success) {
                const user = { ...body }
                var data = user.imageb64.replace(/^data:image\/\w+;base64,/, "");
                var buf = new Buffer.from(data, 'base64');
                // const emailB64 = Buffer.from(body.email).toString('base64');
                user.referredBy = user.referredBy.trim();
                if (user.referredBy) {
                    const userReferred = await app.models.User.findOne({ where: { username: user.referredBy } });
                    if (userReferred) {
                        user.referredId = userReferred.id;
                    }
                }
                const userCreated = await app.models.InfoUser.create(user);
                fs.writeFile('uploads/' + userCreated.id + '.png', buf, err => {
                    if (err) throw err;
                });
                app.io.emit('infoUsers');
                user.img = urlApi + '/api/images?name=' + userCreated.id;
                const confirmLink = null;//urlApi + '/api/Infousers/' + emailB64 + '/backgroundcheck/' + body.language;
                let renderer = loopback.template(path.resolve(__dirname, '../../server/views/inforegister.ejs'));
                let html_body = renderer({ user, confirmLink, lang: body.language });
                const subject = body.language == 'es' ? 'Registro información personal' : 'Register Personal information';
                app.models.Email.send(
                    {
                        to: "luiscurbano@ttvip9108.com",
                        from: '"T&T vip" <noreplay@ttvip9108.com>',
                        subject: subject,
                        html: html_body
                    },
                    (err, mail) => {
                        if (err) console.log('Error on sending email!');
                        console.log('email register sent!');
                    }
                );
                return 'success';
            }
        } catch (err) {
            console.log(err);
            throw new Error('ERROR');
        }
    }

    Infouser.remoteMethod('contactform', {
        http: { path: '/contactform', verb: 'post' },
        accepts: [
            { arg: 'req', type: 'object', http: { source: 'req' } },
            { arg: 'body', type: 'object', http: { source: 'body' } }
        ],
        returns: { type: 'object', root: true }
    });
    Infouser.contactform = async function (req, body) {
        try {
            let res = await recaptcha.verify(body.recaptcha);
            if (res.data.success) {
                const user = { name: body.contact_name, email: body.contact_email, message: body.contact_message }
                console.log(user)
                let renderer = loopback.template(path.resolve(__dirname, '../../server/views/inforegister.ejs'));
                let html_body = renderer({ user, confirmLink: null, lang: body.language });
                const subject = body.language == 'es' ? 'Habla con Nosotros' : 'Talk to Us';
                app.models.Email.send({
                    to: "luiscurbano@ttvip9108.com",
                    from: '"T&T vip" <ttvip9108@gmail.com>',
                    subject: subject,
                    html: html_body
                }, function (err, mail) {
                    if (err) console.log('Error on sending email contact!', err);
                    console.log('email contact sent!');
                });
                return 'success';
            }
        } catch (err) {
            console.log(err);
            throw new Error('ERROR');
        }
    }

    Infouser.remoteMethod('backgroundcheck', {
        http: { path: '/:emailB64/backgroundcheck/:lang', verb: 'get' },
        accepts: [
            { arg: 'req', type: 'object', http: { source: 'req' } },
            { arg: 'emailB64', type: 'string', required: true },
            { arg: 'lang', type: 'string', required: true }
        ],
        returns: { type: 'object', root: true }
    });
    Infouser.backgroundcheck = async function (req, emailB64, lang) {

        const userEmail = Buffer.from(emailB64, 'base64').toString();
        let userBack = await app.models.InfoUser.findOne({ where: { email: userEmail } });
        userBack.updateAttributes({
            confirmBack: true
        });
        let renderer = loopback.template(path.resolve(__dirname, '../../server/views/backgroundcheck.ejs'));
        let html_body = renderer({ message: '', lang: lang });
        const subject = lang == 'es' ? 'Debe realizar verificación de antecedentes' : 'Must run a background check';
        app.models.Email.send({
            to: userEmail,
            from: '"T&T vip" <ttvip9108@gmail.com>',
            subject: subject,
            html: html_body
        }, function (err, mail) {
            if (err) console.log('Error on sending background check email!');;
            console.log('email backgroundcheck sent!');
        });
        return 'success';
    }

    Infouser.remoteMethod('generateInvoice', {
        http: { path: '/:id/generateInvoice', verb: 'get' },
        accepts: [
            { arg: 'req', type: 'object', http: { source: 'req' } },
            { arg: 'res', type: 'object', http: { source: 'res' } },
            { arg: 'id', type: 'string', required: true }
        ],
        returns: { type: 'object', root: true }
    });
    Infouser.generateInvoice = async function (req, res, id) {

        const worker = await app.models.InfoUser.findOne({ where: { id: id } });

        const fee = 500;

        let timeWorked = 0;

        if (worker.received) {
            const received = new Date(worker.received);
            received.setHours(0, 0, 0);
            const endDate = worker.finished ? new Date(worker.finished) : new Date();
            endDate.setHours(0, 0, 0);
            const diffTime = endDate.getTime() - received.getTime();
            timeWorked = diffTime / (1000 * 3600 * 24);
        } else {
            console.log('Error worker not received');
        }

        const total = timeWorked < 15 ? fee * 0.6 : fee;

        const newInvoice = await app.models.Invoice.create({
            items: [{ description: 'Fee Service', price: total }],
            total: total,
            infoUserId: worker.id
        });

        worker.updateAttributes({
            invoiceId: newInvoice.id
        });
        app.io.emit('infoUsers');

        return newInvoice;
    }

    Infouser.remoteMethod('generateInvoiceGlobal', {
        http: { path: '/generateInvoiceGlobal', verb: 'get' },
        accepts: [
            { arg: 'req', type: 'object', http: { source: 'req' } },
            { arg: 'res', type: 'object', http: { source: 'res' } },
        ],
        returns: { type: 'object', root: true }
    });
    Infouser.generateInvoiceGlobal = async function (req, res) {

        const workers = await app.models.InfoUser.find({ where: { finished: null, invoiceId: null, invoiceGlobalId: null } });

        const items = [];

        const fee = 500;

        let total = 0;

        const newInvoiceGlobal = await app.models.Invoice.create({
            items: items,
            total: total,
            infoUserId: 0
        });

        workers.forEach(worker => {

            if (worker.received && !worker.isOnlyIndividual) {
                
                let timeWorked = 0;

                if (!worker.isInvoice100) {
                    const received = new Date(worker.received);
                    received.setHours(0, 0, 0);
                    const endDate = new Date();
                    endDate.setHours(0, 0, 0);
                    const diffTime = endDate.getTime() - received.getTime();
                    timeWorked = diffTime / (1000 * 3600 * 24);
                }

                const price = !worker.isInvoice100 && (timeWorked < 15) ? fee * 0.6 : fee;
                total += price;
                const description = 'Fee Service: ' + worker.id + ' ' + worker.name + ' ' + worker.lastname
                items.push({ description, price });

                worker.updateAttributes({
                    invoiceGlobalId: newInvoiceGlobal.id
                });
            }
        });

        newInvoiceGlobal.updateAttributes({
            items: items,
            total: total
        });
        
        return newInvoiceGlobal;
    }

    Infouser.afterRemote('replaceOrCreate', async function(context, userInstance, next) {
        app.io.emit('infoUsers');
    });

    Infouser.remoteMethod('downloadInvoice', {
        http: { path: '/:id/downloadInvoice', verb: 'get' },
        accepts: [
            { arg: 'req', type: 'object', http: { source: 'req' } },
            { arg: 'res', type: 'object', http: { source: 'res' } },
            { arg: 'id', type: 'string', required: true }
        ],
        returns: { type: 'object', root: true }
    });
    Infouser.downloadInvoice = async function (req, res, id) {

        const invoice = await app.models.Invoice.findOne({ where: { id: id } });
        const worker = invoice.infoUserId ? await app.models.InfoUser.findOne({ where: { id: invoice.infoUserId } }) : null;
        invoice.infoUser = worker;
        let renderer = loopback.template(path.resolve(__dirname, '../../server/views/invoice.ejs'));
        let html_body = renderer({ data: invoice });
        let buffer;
        try {
            buffer = await new Promise((resolve, reject) => {
                pdf.create(html_body, { format: 'Letter' }).toBuffer(function (err, buffer) {
                    if (err) {
                        reject(err);
                    }
                    resolve(buffer);
                });
            });
        } catch (err) {
            console.log(err);
            throw new Error("ERROR");
        }

        res.contentType("application/pdf");
        res.status(200).send(buffer);
    }
};
